package com.certification.devsavant.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatDate {
    private String date;

    public FormatDate() {
    }

    public FormatDate(String date) {
        this.date = date;
    }

    public String toTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("HH:mm");
        Date d = null;
        try {
            d = sdf.parse(this.date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return output.format(d);
    }
}
