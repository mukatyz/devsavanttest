package com.certification.devsavant.utils;

public class Constants {
    public static final String URL_BASE_TIME_ZONES = "http://worldtimeapi.org/api/timezone";
    public static final String URL_BASE_IP = "http://worldtimeapi.org/api/ip/";
    public static final String TIME_ZONE_LIST = "timeZonesList";
    public static final String POSITION_NUMBER = "position number";
    public static final String REGION_NAME = "region name";
    public static final String SAME = "same";
    public static final String CURRENT = "current location";

    public static final String CLIENT_IP = "client_ip";

    public static final String TIME = "datetime";
    public static final String TIME_ZONE = "timezone";
}
