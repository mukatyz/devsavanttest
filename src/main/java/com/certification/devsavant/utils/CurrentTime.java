package com.certification.devsavant.utils;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.actors.OnStage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.stream.Collectors;

import static com.certification.devsavant.utils.Constants.SAME;

public class CurrentTime {
    String timeZone;

    public CurrentTime(){

    }
    public CurrentTime(String timeZone){
        this.timeZone = timeZone;
    }

    public String getTimeZone(){
        String region = OnStage.theActorInTheSpotlight().recall("region");
        if(this.timeZone.equals(SAME)){
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            if (region.contains("txt")){
                String arraylist[] =SerenityRest.lastResponse().getBody().print().split("\n");
                sdf.setTimeZone(TimeZone.getTimeZone(Arrays.stream(arraylist).filter(a -> a.contains("timezone")).collect(Collectors.toList()).get(0).replaceAll("timezone: ","")));
            }else{
                sdf.setTimeZone(TimeZone.getTimeZone(SerenityRest.lastResponse().jsonPath().getString("timezone")));
            }

            return sdf.format(GregorianCalendar.getInstance().getTime());
        }
        else {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            sdf.setTimeZone(TimeZone.getTimeZone(this.timeZone));
            return sdf.format(GregorianCalendar.getInstance().getTime());
        }
    }

}
