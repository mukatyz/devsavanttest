package com.certification.devsavant.exceptions;

public class StatusCodeException extends AssertionError{
    public StatusCodeException(String errorMessage){
        super(errorMessage);
    }
}
