package com.certification.devsavant.userinterfaces;


import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MainSreenComponents {
    public static final Target SEARCH_BOX = Target.the("Type the keyword").
            located(By.name("q"));
    public static final Target FEELING_LUCKY_BUTTON = Target.the("I'm Feeling Lucky button").
            locatedBy("(//*[@class=\"RNmpXc\"])[1]");
}
