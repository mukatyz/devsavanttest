package com.certification.devsavant.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Get;

import static com.certification.devsavant.utils.Constants.*;

public class ObtainInformation implements Task {
    private String ip;

    public ObtainInformation(String ip) {
        this.ip = ip;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        if(ip.equals(CURRENT))
            ip = "";
        actor.whoCan(CallAnApi.at(URL_BASE_IP + ip));
        actor.attemptsTo(Get.resource(""));
    }
    public static ObtainInformation fromIpRequest(String ip){
        return Tasks.instrumented(ObtainInformation.class, ip);
    }


}