package com.certification.devsavant.tasks;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Get;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static com.certification.devsavant.utils.Constants.*;

public class ObtainSpecificTimeZone implements Task {
    private String region;
    private String filterType;

    public ObtainSpecificTimeZone(String filterType,String region) {
        this.filterType = filterType;
        this.region = region;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        switch (this.filterType) {
            case POSITION_NUMBER:
                ArrayList<String> timeZonesList = actor.recall(TIME_ZONE_LIST);
                String regionName = timeZonesList.get(Integer.parseInt(this.region)-1);
                actor.whoCan(CallAnApi.at(URL_BASE_TIME_ZONES + "/"+ regionName));
                actor.attemptsTo(Get.resource(""));
                break;
            case REGION_NAME:
                actor.whoCan(CallAnApi.at(URL_BASE_TIME_ZONES + "/" + this.region));
                actor.attemptsTo(Get.resource(""));
                break;
        }


    }
    public static ObtainSpecificTimeZone withTheData(String filterType, String region){
        return Tasks.instrumented(ObtainSpecificTimeZone.class,filterType,region);
    }



}
