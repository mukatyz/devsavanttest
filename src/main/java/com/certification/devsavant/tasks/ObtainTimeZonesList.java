package com.certification.devsavant.tasks;

import com.certification.devsavant.exceptions.StatusCodeException;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.*;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Get;

import static com.certification.devsavant.utils.Constants.*;

public class ObtainTimeZonesList implements Task{
    public ObtainTimeZonesList() {
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.whoCan(CallAnApi.at(URL_BASE_TIME_ZONES));
        actor.attemptsTo(Get.resource(""));
        if(SerenityRest.lastResponse().statusCode() != 200)
                throw new StatusCodeException("The request is not responding 200 as status code, is responding: " + SerenityRest.lastResponse().statusCode());
        actor.remember(TIME_ZONE_LIST,SerenityRest.lastResponse().jsonPath().getList(""));
    }

    public static ObtainTimeZonesList entire() {
        return Tasks.instrumented(ObtainTimeZonesList.class);
    }

}


