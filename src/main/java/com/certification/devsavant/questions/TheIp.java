package com.certification.devsavant.questions;

import com.certification.devsavant.utils.FormatDate;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.actors.OnStage;

import java.util.Arrays;
import java.util.TimeZone;
import java.util.stream.Collectors;

import static com.certification.devsavant.utils.Constants.TIME;

public class TheIp implements Question<String> {
    private String consultType;

    public TheIp(String consultType) {
        this.consultType = consultType;
    }

    @Override
    public String answeredBy(Actor actor) {
        return SerenityRest.lastResponse().jsonPath().getString(consultType);
    }

    public static TheIp information(String consultType){
        return new TheIp(consultType);
    }
}
