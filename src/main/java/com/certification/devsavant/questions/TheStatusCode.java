package com.certification.devsavant.questions;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class TheStatusCode implements Question<Integer> {
    /**
     * @param actor
     * @return
     */
    @Override
    public Integer answeredBy(Actor actor) {
        return SerenityRest.lastResponse().statusCode();
    }
    public static TheStatusCode obtained(){
        return new TheStatusCode();
    }
}
