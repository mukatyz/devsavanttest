package com.certification.devsavant.questions;

import com.certification.devsavant.utils.FormatDate;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.actors.OnStage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

public class TheTime implements Question<String> {

    @Override
    public String answeredBy(Actor actor) {
        String region = OnStage.theActorInTheSpotlight().recall("region");
        String dateTime;
        if (region.contains("txt")){
            String arraylist[] =SerenityRest.lastResponse().getBody().print().split("\n");
            dateTime =Arrays.stream(arraylist).filter(a -> a.contains("datetime: ")).collect(Collectors.toList()).get(0).replaceAll("datetime: ","");
        }else{
            dateTime = SerenityRest.lastResponse().jsonPath().getString("datetime");
        }
        FormatDate formatDate = new FormatDate(dateTime);
        return formatDate.toTime();
    }

    public static TheTime obtained(){
        return new TheTime();
    }


}
