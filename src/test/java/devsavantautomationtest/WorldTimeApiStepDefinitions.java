package devsavantautomationtest;

import com.certification.devsavant.exceptions.StatusCodeException;
import com.certification.devsavant.questions.TheIp;
import com.certification.devsavant.questions.TheStatusCode;
import com.certification.devsavant.questions.TheTime;
import com.certification.devsavant.tasks.ObtainInformation;
import com.certification.devsavant.tasks.ObtainSpecificTimeZone;
import com.certification.devsavant.tasks.ObtainTimeZonesList;
import com.certification.devsavant.utils.CurrentTime;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import org.hamcrest.Matchers;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class WorldTimeApiStepDefinitions {
    @Given("^The user obtains the list of time zones$")
    public void the_user_obtains_the_list_of_time_zones() {
        theActorInTheSpotlight().attemptsTo(ObtainTimeZonesList.entire());
    }
    @When("^takes the time zone with a (.*): (.*)$")
    public void takes_the_time_zone(String filterType, String region) {
        theActorInTheSpotlight().remember("region",region);
        theActorInTheSpotlight().attemptsTo(ObtainSpecificTimeZone.withTheData(filterType,region));
    }
    @When("^gets the current time for the (.*) time zone$")
    public void gets_the_current_time_for_the_same_time_zone(String regionToCompare) {
        CurrentTime timeKaty = new CurrentTime(regionToCompare);
        theActorInTheSpotlight().remember("time", timeKaty.getTimeZone());
    }
    @Then("^should see the both time are equal$")
    public void should_see_the_both_time_are_equal() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(TheTime.obtained(), Matchers.is(Matchers.equalTo(theActorInTheSpotlight().recall("time")))));
    }
    @Then("^should see the both time are not equal$")
    public void should_see_the_both_time_are_not_equal() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(TheTime.obtained(), Matchers.not(Matchers.equalTo(theActorInTheSpotlight().recall("time")))));
    }

    @Given("^The user try to obtain the list of (.*)")
    public void theUserObtainsTheListOfIp(String kindOfIp) {
        theActorInTheSpotlight().attemptsTo(ObtainInformation.fromIpRequest(kindOfIp));
    }
    @Then("^the (.*) should be (.*)$")
    public void theIpShouldBeTxt(String consultType, String expectedValue) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(TheIp.information(consultType),Matchers.is(Matchers.equalTo(expectedValue))));
    }
    @Then("^the response should have the (.*)$")
    public void theStatusCodeShouldBe(Integer expectedStatusCode) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(TheStatusCode.obtained(),Matchers.is(Matchers.equalTo(expectedStatusCode))).orComplainWith(StatusCodeException.class,"The Status code was wrong"));
    }
}
