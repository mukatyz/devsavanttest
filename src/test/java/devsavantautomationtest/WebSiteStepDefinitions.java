package devsavantautomationtest;

import com.certification.devsavant.userinterfaces.MainSreenComponents;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;

public class WebSiteStepDefinitions {

    @Given("the user open the web pag")
    public void theUserOpenTheWebPag() {
        OnStage.theActorInTheSpotlight().attemptsTo(Open.url("http://www.google.com/"));
    }

    @When("type the Devsavant")
    public void typeTheDevsavant() {
        OnStage.theActorInTheSpotlight().attemptsTo(Enter.theValue("Devsavant").into(MainSreenComponents.SEARCH_BOX));

    }

    @When("click on I'm Feeling Lucky")
    public void clickOnIMFeelingLucky() {
        OnStage.theActorInTheSpotlight().attemptsTo(Click.on(MainSreenComponents.FEELING_LUCKY_BUTTON));
    }
}
