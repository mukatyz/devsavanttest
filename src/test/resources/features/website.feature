@WebSite
Feature: As a www.google.com user
  I want to type a keyword in the search box and click on the "I'm Feeling Lucky" button
  So that I would be taken directly to the most relevant result

  Scenario Outline: navigate in google and search with a keywork
    Given the user open the web pag
    When type the <keyword>
    And click on I'm Feeling Lucky
    Examples:
      | keyword|
      | Devsavant|


