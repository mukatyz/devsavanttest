# Devsavant Automation Test

This project contains a structure ready to implement automated test cases using Cucumber and Selenium along with the following components:

- JUnit Assert
- Cucumber
- Selenium Webdriver
- Serenity BDD
- Screenplay
- Chrome Driver
- java.net.http
- org.json.simple

# Description
 
In the route src\test\resources\features you should find two features files, where is drescribed the step by step to validate the World Time Api and for opening a web page using a keyword.

# How to run the project 

The commands used to run the project, the opening the Terminal in the project root:

- All the test cases: .\gradlew clean test aggregate -info
- Api tests: .\gradlew clean test -Dcucumber.options="--tags '@WorldTimeApi'" aggregate -info
- Web page tests: .\gradlew clean test -Dcucumber.options="--tags '@WebSite'" aggregate -info
- Scenarios Outline: .\gradlew clean test -Dcucumber.options="--tags '@ScenarioOutline'" aggregate -info

When the executions finish you should go to the folder cd devsavant-automation-test/target/site/serenity/index.html
to see the execution report with the devsavant screen capture



