@WorldTimeApi
Feature: As certification analyst
  I need to validate the functionalities of world time api

  Scenario: Verify time zone functionality with a position number
    Given The user obtains the list of time zones
    When takes the time zone with a position number: 45
    And gets the current time for the same time zone
    Then should see the both time are equal

  Scenario: Verify time zone functionality with a region name in the same region
    Given The user obtains the list of time zones
    When takes the time zone with a region name: Europe/London
    And gets the current time for the same time zone
    Then should see the both time are equal

  Scenario: Verify time zone functionality with a region name with a different region
    Given The user obtains the list of time zones
    When takes the time zone with a region name: Europe/London
    And gets the current time for the Argentina/Salta time zone
    Then should see the both time are not equal

  Scenario: Verify time zone functionality with a region name in the same region
    Given The user obtains the list of time zones
    When takes the time zone with a region name: Europe/London.txt
    And gets the current time for the same time zone
    Then should see the both time are equal

  Scenario: Verify time zone functionality with a region name with a different region
    Given The user obtains the list of time zones
    When takes the time zone with a region name: Europe/London.txt
    And gets the current time for the Argentina/Salta time zone
    Then should see the both time are not equal

  @ScenarioOutline
  Scenario Outline: Verify status code if the request is malformed
    Given The user try to obtain the list of <ip>
    Then the response should have the <status code>
    Examples:
      | ip               | status code |
      | 8.8.8.8          | 200         |
      | 8.8.8.x          | 404         |
  @ScenarioOutline
  Scenario Outline: Verify ip functionality in the current ip
    Given The user try to obtain the list of <ip>
    Then the client_ip should be <client ip expected>
    And the timezone should be <time zone expected>
    Examples:
      | ip               | client ip expected | time zone expected |
      | current location | 186.159.4.171      | America/Bogota     |
      | 8.8.8.8          | 186.159.4.171      | America/Chicago    |







